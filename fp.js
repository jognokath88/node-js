const sub = x => x - 1;
const divide = y => y / 2;

const compose = ( x, y ) => ( z ) => x( y( z ) );

const subDivide = compose(sub, divide);

subDivide(10);

// currying
function add(n) {
    return function(m) {
        return m + n;
    }
}

const increment = add(1);
increment(5); // 6
increment(8); // 9

const decrement = add(-1);
decrement(11); // 10
decrement(8); // 7

// map
const nums = [1, 2, 3, 4, 5];
const double = nums.map(n => n*2);

console.log(double) // [ 2, 4, 6, 8, 10 ]

// filter
const isEven = (num) => num %2 === 0;

const evens = nums.filter(n => isEven(n) );
console.log(evens) // [ 2, 4, ]


// reduce

const totalSum = nums.reduce((i, n) => {
    return i + n;
}, 0);

const ave = totalSum / nums.length; // 15 / 5
console.log(ave) // 3
