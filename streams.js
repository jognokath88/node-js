const fs = require('fs');

// duplex
const rdrStrm = fs.createReadStream('result.txt');
const wrtrStrm = fs.createWriteStream('new_output.txt');

rdrStrm.pipe(wrtrStrm);

rdrStrm.on('error', (e) => {
    console.error(e.stack);
})

console.log('done duplex programs');

/*
let words = '';
const rdrStrm = fs.createReadStream('result.txt');

rdrStrm.setEncoding('UTF-8'); // set encoding

rdrStrm.on('data', (chunks) => {
    words += chunks;
});

rdrStrm.on('end', () => {
    console.log(words);
});

rdrStrm.on('error', (e) => {
    console.error(e.stack);
});

console.log('done reading program')*/
/* write stream
let words = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.';

const wrStrm = fs.createWriteStream('result.txt');

wrStrm.write(words, 'UTF-8'); // sync
 
wrStrm.end(); // sync

wrStrm.on('finish', () => {
    console.log('writing is done'); 
});

wrStrm.on('error', (e) => {
    console.error(e.stack);

});

console.log('End of program')*/