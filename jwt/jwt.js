const jwt = require('jsonwebtoken');
const koaJwt = require('koa-jwt');
const dotenv = require('dotenv-safe');

dotenv.config()

const secret = process.env['JWT_SECRET']

const generateToken = (usr) => {
    return jwt.sign({
        usr,
        iat: Math.floor( ( Date.now() / 1000 ) + 3600) 
    }, secret)
} 
const jwtMiddleware = () => { return koaJwt({secret: secret}) }

module.exports = {
    generateToken,
    jwtMiddleware
}
