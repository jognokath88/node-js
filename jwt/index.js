const Koa = require('koa');
const Router = require('koa-router');
const helmet = require('koa-helmet');
const kb = require('koa-body');
const jwt = require('./jwt')
const logger = require('koa-logger');
const { isAuthorized } = require('./user');
const generateToken = jwt.generateToken;
const jwtMiddleWare = jwt.jwtMiddleware;
const jsonBody = kb.koaBody;

const app = new Koa();
const router = new Router();

const port = process.env.PORT || 8080;

router
    .get('/', async (ctx) => {
        ctx.status = 200;
        ctx.body = { message: 'accesssed public route'};
    })
    .post('/login',  async(ctx) => {
        const data = ctx.request.body || ctx.req.body; // ctx.req.body is parsed already
        // validate user/pass
        if (!isAuthorized(data['username'], data['password'])) {
            ctx.status = 401;
            ctx.body = {message : 'Wrong username or password'};
            return;
        } 

        ctx.status = 200;
        
        // return jwt token
        ctx.body = { token: generateToken(data['username']) }
    })
    .get('/private', jwtMiddleWare(), async (ctx) => {
        ctx.status = 200;
        ctx.body = {message: 'accessed private route'};
    })

// handle unauthorized access, 401 error
app.use( async (ctx, next) => {
    return next().catch((err) => {

        if (err.status === 401) {
            ctx.status = 401;
            ctx.body = {message:'Unauthorized'};
        } else {
            throw err;
        }
    });
})

app
    .use(logger())
    .use(jsonBody())
    .use(helmet())
    .use(router.routes())
    .use(router.allowedMethods())
    .listen(port, () => {
        console.log(`listening on ${port}`)
    });