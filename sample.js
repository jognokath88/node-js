const fs = require('fs');
const buffer = Buffer.alloc(10) // buffer of length 10 octets
console.log(buffer) // <Buffer 00 00 00 00 00 00 00 00 00 00>

console.log(fs.readFileSync('new.txt')) // <Buffer 48 65 6c 6c 6f 20 77 6f 72 6c 64 0d 0a 4c 6f 72 65 6d 20 49 70 73 75 6d 20 69 73 20 73 69 6d 70 6c 79 20 64 75 6d 6d 79 20 74 65 78 74 20 6f 66 20 74 ... 537 more bytes>

console.log(fs.readFileSync('new.txt').toString('utf-8')
) /* Hello world
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. */

const os = require('os'); 
// console.log(`CPU Time ${os.uptime()}`) // CPU Time 2541.88

// console.log(`AVG CPU load at 1/5/15 ${os.loadavg()}`) //  0,0,0

// console.log(`Info about each CPU ${JSON.stringify(os.cpus())}`) // Info about each CPU  [{"model":"11th Gen Intel(R) Core(TM) i5-11300H @ 3.10GHz","speed":3110,"times":{"user":2220,"nice":0,"sys":2110,"idle":2628110,"irq":0}},{"model":"11th Gen Intel(R) Core(TM) i5-11300H @ 3.10GHz","speed":3110,"times":{"user":2020,"nice":0,"sys":1160,"idle":2629500,"irq":0}},{"model":"11th Gen Intel(R) Core(TM) i5-11300H @ 3.10GHz","speed":3110,"times":{"user":1510,"nice":0,"sys":1190,"idle":2629810,"irq":0}},{"model":"11th Gen Intel(R) Core(TM) i5-11300H @ 3.10GHz","speed":3110,"times":{"user":1290,"nice":0,"sys":670,"idle":2630520,"irq":0}},{"model":"11th Gen Intel(R) Core(TM) i5-11300H @ 3.10GHz","speed":3110,"times":{"user":4020,"nice":0,"sys":1180,"idle":2628520,"irq":0}},{"model":"11th Gen Intel(R) Core(TM) i5-11300H @ 3.10GHz","speed":3110,"times":{"user":900,"nice":0,"sys":990,"idle":2630890,"irq":0}},{"model":"11th Gen Intel(R) Core(TM) i5-11300H @ 3.10GHz","speed":3110,"times":{"user":3800,"nice":0,"sys":4040,"idle":2621900,"irq":0}},{"model":"11th Gen Intel(R) Core(TM) i5-11300H @ 3.10GHz","speed":3110,"times":{"user":760,"nice":0,"sys":1230,"idle":2631790,"irq":0}}]

const dns = require('dns');

dns.resolve4('google.com', (err, addresses) => {
    if (err) {
        throw err
    } else {
        console.log('addresses: ' + JSON.stringify(addresses))
    }
}) // usually a lot of output

// import fs from 'fs' ;
/*const fs = require('fs');
const someText = 'icecream'
const folder = 'kwekkwek'
const txtFile = 'new2.txt'
fs.mkdir(folder, () => {
    fs.writeFile(`${folder}/${txtFile}`, someText, (err) => {
        fs.readFile(`${folder}/${txtFile}`, (err, data) => {
            console.log( data.toString('utf8') );
        })
    })
});*/

// console.log('before readfile')
// fs.readFile('new.txt', (err, data) => {

//     if (err) {
//         console.error(err.message);
//         throw err;
//     } 
//     console.log('reading readfile')

//     console.log(data.toString('utf-8'));
// })
// console.log('after readfile')

/*import http from 'http';
// const http = require('http');
const hostname = 'localhost';
const port = 3000;

const server = http.createServer( (req, res) => {
    console.log(req.headers)

    res.writeHead( 200, {'Content-type': 'text/html'} )
    res.write('hello achievers!')
    res.end('<h1> Hello world</h1>')
})

server.listen( port, hostname, () => {
    console.log(`check http://${hostname}:${port}`)
})*/

// console.log(__dirname);
// console.log(__filename);

// setTimeout( function() {
//     console.log('ready set go')
// }, 3000);

// console.log(process.cwd())

// console.log(process.cpuUsage())

// console.log(process.argv)  // cmdline options