const fs = require('fs');
const koa = require('koa');
const http2 = require('http2');

const app = new koa();

const HTTP_PORT = 8000;
const HTTP2_PORT = 8001;

const key1 = fs.readFileSync('./key.pem');
const cert1 = fs.readFileSync('./cert.pem');

const options = {
    key: key1,
    cert: cert1,
}

app.use(ctx => {
    ctx.body = 'Learning about certificate signing request...';
});

// fallback unsecured server
app.listen(HTTP_PORT, () => {
    console.log(`Ready unsecured: ${HTTP_PORT}`);
})

// set http2 server
http2.createSecureServer(options, app.callback())
    .listen(HTTP2_PORT, ()=> {
        console.log(`Ready secured: ${HTTP2_PORT}`);
})

