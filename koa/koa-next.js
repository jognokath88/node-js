const koa = require('koa');
const app = new koa();

app.use(async (ctx, next) => {
    console.log('>> one');
    await next();
    console.log('>> ??');
})

app.use(async (ctx, next) => {
    console.log('>> two');
    ctx.body = 'two';
    await next(); // added this line to output console log in order of one two three
})

app.use(async (ctx, next) => {
    console.log('>> three');
    await next();
})

app.listen(3000);