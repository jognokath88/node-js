// require koa
const koa = require('koa');
const serve = require('koa-static-server');
// initialize koa
const app = new koa(); 

const hostname = 'localhost';
const port = 3000;

app.use( serve( {rootDir: './public/'} ) );

// connect to a port
app.listen(port, hostname, () => {

    console.log(`Server at http://${hostname}:${port}/`);
});