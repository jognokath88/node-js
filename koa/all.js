const Koa = require('koa');
const Router = require('koa-router');

const app = new Koa();
const router = new Router();

// this route will match anything with kwek
router.get(/kwek/, ctx => {
    ctx.status = 200;
    ctx.body = 'hello world';
}) 

router.all('/private', ctx => {
    console.log('Rerouting...');
    ctx.redirect('/login');
    ctx.status =  301;
})

app.use( router.routes() );

app.listen(3000);