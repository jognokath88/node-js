const Koa = require('koa');
const Router = require('koa-router');

const app = new Koa();
const router = new Router();

router.get('/kwekkwek', ctx => {
    const data = 'I want to eat kwekkwek';
    ctx.status = 200;
    ctx.body = data;

})

app.use( router.routes() );
app.listen(8080);