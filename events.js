const { hasSubscribers } = require('diagnostics_channel');
const events = require('events');

const eventEmitter = new events.EventEmitter();

const eventHandler = () => {
    console.log('Event!')
    eventEmitter.emit('event_received')
}

eventEmitter.on('event_received', () => {
    console.log('Event received successfully')
}) 

// const myEvent = eventEmitter;

eventEmitter.on('this_event', eventHandler) // handle
eventEmitter.emit('this_event'); // turn on

//Event executed successfully
// myEventEmitter.emit('evnt_executed',1,2,3,4,5)
// create , handle and turn it on

